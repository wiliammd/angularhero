import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PoI18nService, PoMenuItem, PoToolbarAction, PoToolbarProfile } from '@po-ui/ng-components';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  profile: PoToolbarProfile;
  profileActions: Array<PoToolbarAction>;
  literals: any;
  menus: Array<PoMenuItem>;

  mostrarMenu: boolean = false;

  constructor(private poI18nService: PoI18nService,
    private router: Router,
    private location: Location,
  ) {

    this.poI18nService.getLiterals({ language: 'pt-BR' }).subscribe((literals) => {
      this.literals = literals;
    });
  }

  ngOnInit(): void {
    if (true) {
    }
    else {
    }
    this.mostrarMenu = true;
    this.menus = [
      {
        label: this.literals['home'],
        shortLabel: this.literals['home'],
        icon: 'po-icon po-icon-home',
        link: '/'
      },
    ];


  }


}
