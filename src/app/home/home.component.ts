import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoDynamicViewField, PoI18nService, PoNotification, PoNotificationService } from '@po-ui/ng-components';
import { Hero } from 'src/app/models/hero/hero.dto';
import { Powerstats } from './../models/hero/hero.dto';
import { HeroGetService } from './../services/hero/hero-get-provider';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  literals: any;
  id:number;
  logado:boolean;
  poNotification: PoNotification;
  token:string;
  testeToken:String;
  dynimcField:Array<PoDynamicViewField>= [];
  disableButton:boolean=true;
  stringUrl:string;
  hero= {} as Hero;
  hasHero:boolean= false;
  heroStatus = {} as Powerstats;
  menus: Array<any>;
  constructor(
    private poI18nService: PoI18nService,
    private notification: PoNotificationService,
    private router: Router,
    private heroGetService:HeroGetService,
  ) {
    this.poI18nService.getLiterals({ language: 'pt-BR' }).subscribe((literals) => {
      this.literals = literals;
  })
}


  ngOnInit(): void {
    this.menus = [];
    if(localStorage.getItem('token') == null){
      this.logado=false;
      this.router.navigate(['/login']);
    }else{
      this.router.navigate(['/home']);
      this.logado=true;
    }
    this.dynimcField = [ { property: 'id' } ,
    { property: 'name' },

     ]
  }

  async FindHeroById(){
    if(this.id !=null)
    this.heroGetService.getHeroById(this.id).subscribe((hero: Hero) => {
      this.hero = hero;
      this.heroStatus = this.hero.powerstats;
      this.hasHero=true;
      this.stringUrl = this.hero.image.url;
      if(this.hero.response !='success'){
        this.notification['error'](this.literals.tokenInvalido);
        this.logout();
      }
      console.log(this.hero);
    });
}
logout(){
  localStorage.removeItem('token');
  this.logado=false;
  this.navPlink('login');
}

  navPlink(link: string) {
    this.router.navigate([`${link}`]);
  }

  login(){
    localStorage.setItem('token',this.token);
    console.log(localStorage.getItem('token'));
    this.logado=true;
    this.navPlink('home');
  }
  existNumber(event){
    this.testeToken = event;
    this.token = event.toString();
    if(this.testeToken.length>15){
      this.disableButton=false;
    }else{
      this.disableButton=true;
    }
  }



}
